﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{

    public GameObject Key;
    public GameObject Exit;
    public GameObject Escape;

    bool m_IsKeyCollected;
    

     void Start ()
    {
        m_IsKeyCollected = false;
    }  
    void onTriggerEnter(Collider other)
    {  

        if (other.gameObject.CompareTag ("Key"))
        {
            m_IsKeyCollected = true;

        }
        

        if (other.gameObject == Escape)
        {   
                SceneManager.LoadScene ("EndScene", LoadSceneMode.Single);
        }   
    }
    

    void Update()
    {
        if (m_IsKeyCollected)
        {
            Exit.SetActive (false);
            Key.SetActive (false);
        }   

        
    }

}
