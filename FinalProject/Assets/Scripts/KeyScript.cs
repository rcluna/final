﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
  public GameObject Exit;
  public GameObject Player;
  public GameObject Key;

  void onTriggerEnter (Collider other)
   {
       if (other.gameObject == Player)
       {
           Destroy(Key);
           Destroy(Exit);
       }
        
   } 
}
