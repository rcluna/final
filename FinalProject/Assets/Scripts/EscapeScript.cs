﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeScript : MonoBehaviour
{
    public GameObject Player;

    void onTriggerEnter (Collider other)
    {
        if (other.gameObject == Player)
        { 
            SceneManager.LoadScene ("EndScene" , LoadSceneMode.Single);
        }
    }
}
