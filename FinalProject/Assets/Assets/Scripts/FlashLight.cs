﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashLight : MonoBehaviour
{
    public float flashlightEnergy = 10f;
    public GameObject flashLight;
    public Text PowerText;
    public Light Light;
    public Color on;
    public Color off;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PowerText.text = flashlightEnergy.ToString ();
        if (flashlightEnergy <= 0f)
        {
            flashlightEnergy = 0f;
        }

        if (Input.GetKey(KeyCode.F) && flashlightEnergy > 0f)
        {
            Light.color = on;
            flashlightEnergy -= Time.deltaTime;
            
        }
        else
        {
             Light.color = off;
        }
    }
}
